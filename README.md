## 介绍
简单-Admin 是一个基于 SpringBoot 2 的权限管理系统。核心技术采用 Spring、MyBatis、Shiro，没有任何其他重度依赖。可直接运行使用。可以免费商用，无限制。该版本是前后端不分离版本。

如需前后端分离版本，请移步jiandan-admin-vue：[https://gitee.com/javaex/jiandan-admin-vue](https://gitee.com/javaex/jiandan-admin-vue)


### 软件架构
- **JDK**：1.8
- **数据库**：MySQL 8
 
### 后端
- **核心框架**：SpringBoot 2.1.3.RELEASE
- **安全框架**：Shiro
- **数据库操作**：MyBatis + PageHelper + mybatisjj（自研）
- **数据库连接池**：Druid
- **工具类**：FastJSON + htool（自研）
- **导入导出**：officejj（自研）
 
### 前端
- **模板引擎**：FreeMarker
- **UI 库**：jQuery + javaex（自研）
 
## 开发优势
1. 不用写任何注解即可实现菜单的控制权限。
2. 仅需一个注解即可实现数据权限。
3. FreeMarker 模板引擎的语法贴近 JSP，更适合后端快速开发。
4. 所有轮子全都是自研，文档齐全，代码简洁，更容易上手。
 
## 在线体验
- **账号**：test
- **密码**：123456

演示地址：[https://jiandan-demo.javaex.cn](https://jiandan-demo.javaex.cn)

文档地址：[https://doc.javaex.cn/jiandan-admin](https://doc.javaex.cn/jiandan-admin)
 
官网：[https://www.javaex.cn](https://www.javaex.cn)
 
## 演示图
![输入图片说明](upload/1.png)
![输入图片说明](upload/2.png)
![输入图片说明](upload/3.png)
![输入图片说明](upload/4.png)
![输入图片说明](upload/5.png)
![输入图片说明](upload/6.png)
![输入图片说明](upload/7.png)
![输入图片说明](upload/8.png)
![输入图片说明](upload/9.png)
![输入图片说明](upload/10.png)
![输入图片说明](upload/11.png)
![输入图片说明](upload/12.png)
---
